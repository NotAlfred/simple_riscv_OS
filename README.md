# simple riscv kernel

a simple kernel for riscv to learn more about the world of embedded systems.

## TODOs (in order)
- [ ] serial I/O
- [ ] physical memory manager
- [ ] paging
- [ ] heap
- [ ] interrupt/trap handling
- [ ] processes
- [ ] cli
- [ ] text editor
- [ ] init
- [ ] framebuffer
- [ ] window manager
- [ ] terminal