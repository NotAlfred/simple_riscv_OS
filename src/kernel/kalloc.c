#include <stddef.h>
#include <stdint.h>

struct MemoryList {
    uint8_t is_available;
    struct VirtualAddress virt_address;
    struct FreeMemory* next;
};

static struct MemoryList memory_list = {};



// uint64_t
// kalloc(uint32_t size)
// {
//     // getting the number of pages I need to allocate. (if it became zero, turn
//     // it into one)
//     size_t number_of_pages = size / (1 << 12); // each page is 4096 bytes

//     // check if we have available memories in our FreeMemory list.


//     // return fetch(number_of_pages);
// }
