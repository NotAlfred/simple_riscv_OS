struct PageTableEntry {
    unsigned int v : 1;
    unsigned int r : 1;
    unsigned int w : 1;
    unsigned int x : 1;
    unsigned int u : 1;
    unsigned int g : 1;
    unsigned int a : 1;
    unsigned int d : 1;
    unsigned int rsw : 2;
    unsigned int ppn0 : 9;
    unsigned int ppn1 : 9;
    unsigned int ppn2 : 26;
    unsigned int reserved : 7;
    unsigned int pbmt : 2;
    unsigned int n : 1;
} __attribute__((packed));

struct VirtualAddress {
    unsigned int page_offset : 12;
    unsigned int vpn0 : 9;
    unsigned int vpn1 : 9;
    unsigned int vpn2 : 9;
    unsigned int vpn3 : 9;
    unsigned int vpn4 : 9;
} __attribute__((packed));

struct PhysicalAdress {
    unsigned int page_offset : 12;
    unsigned int ppn0 : 9;
    unsigned int ppn1 : 9;
    unsigned int ppn2 : 9;
    unsigned int ppn3 : 9;
    unsigned int ppn4 : 8;
} __attribute__((packed));

struct PageTable {
    struct PageTableEntry pte[1 << 9];
};
