#include <stdint.h>

volatile uint8_t* uart_port = (uint8_t*)0x10000000;

static inline void
putchar(const char c)
{
    *uart_port = c;
}

void
printstr(const char* s)
{
    while (*s != '\0') {
        putchar(*s);
        ++s;
    }
}
